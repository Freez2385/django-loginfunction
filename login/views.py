from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib import auth
from django.http import HttpResponse
from django.contrib import messages

# Create your views here.

def loginindex(request):
    return render(request, 'loginlayout/index.html')


def logincheck(request):
    if request.method == "POST":
        userid = request.POST["id"]
        userpassword = request.POST["password"]
        user = auth.authenticate(request, username = userid, password = userpassword)
        if user is not None:
            auth.login(request, user)
        else:
            messages.info(request, 'Do not match ID or Password')
            return render(request, 'loginlayout/index.html')
    return HttpResponse("Welcome!  " +  request.POST["id"])
