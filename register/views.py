from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib import auth
from django.contrib import messages

# Create your views here.
def registerindex(request):
    return render(request, 'registerlayout/index.html')

def idpasswordcheck(request):
    if request.method == "POST":
        try:
            if request.POST["password"] == request.POST["passwordconfig"]:
                user = User.objects.create_user(
                username = request.POST["id"],
                password = request.POST["password"],
                email = request.POST["email"]
                )
            else:
                messages.info(request, 'Do not match password')
                return render(request, 'registerlayout/index.html')

        except Exception as e:
            messages.info(request, 'There is one more ID exist')
            return render(request, 'registerlayout/index.html')
    messages.info(request, 'Finished!')
    return render(request, 'registerlayout/index.html')
